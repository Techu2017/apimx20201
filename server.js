var express = require('express'),
   app = express(),
   port = process.env.PORT || 3000;

var path = require('path');
var requestjson = require('request-json');

var db= null, collection = null;
var mongodb = require('mongodb');
var Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ReplSetServers = require('mongodb').ReplSetServers,
    ObjectID = require('mongodb').ObjectID,
    Binary = require('mongodb').Binary,
    GridStore = require('mongodb').GridStore,
    Grid = require('mongodb').Grid,
    Code = require('mongodb').Code,
    assert = require('assert');

    MongoClient.connect('mongodb://admin123:admin123@ds163757.mlab.com:63757/fgutierrez', (err,client) => {
      try {
        if(err)
        {
          return console.log("No conecto" + err);
        }
        else {
            console.log("CONEXION BD: OK");
            db = client.db('fgutierrez');
       }
      } catch(err) {
              console.log(err.message)
      }
    });

var urlRaizMlab = "https://api.mlab.com/api/1/databases/fgutierrez/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var ClienteMLabRaiz;

var urlClientes = "https://api.mlab.com/api/1/databases/fgutierrez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlProductos = "https://api.mlab.com/api/1/databases/fgutierrez/collections/Productos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var bodyParser = require('body-parser');
var movimientosJSON= require('./movimientosv2.json');

app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  next();
});



app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/', function(req,res){
 res.send('Servidor Funcionando backend API');
})

/*app.get ('/', function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
})*/

app.get ('/Clientes/:idcliente', function(req,res){
  res.send('Aqui tienes al cliente número: ' + req.params.idcliente);
})

app.post ('/', function(req,res){
  res.send('Hemos recibido su petición post');
})
app.put ('/', function(req,res){
  res.send('Hemos recibido su petición cambiada');
})
app.delete ('/', function(req,res){
  res.send('Petición para borrar');
})

app.get ('/v1/movimientos', function(req,res){
  res.sendfile('movimientos v1.json');
})

app.get ('/v2/movimientos', function(req,res){
  res.json(movimientosJSON);
})

app.get ('/v2/movimientos/:indice', function(req,res){
  console.log(req.parms.indice);
  res.json(movimientosJSON[req.params.indice]);
})

app.get ('/v3/movimientosquery', function(req,res){
  console.log(req.query);
  res.send('Recibido');
})
app.post ('/v3/movimientos', function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta')
})

app.get ('/Clientes', function(req,res){
  var clienteMlab = requestjson.createClient(urlClientes);
  clienteMlab.get('',function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

/* REcupera los datos de los productos desde Momgo*/
app.get ('/Productos', function(req,res){
  var productoMlab = requestjson.createClient(urlProductos);
  productoMlab.get('',function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

/* REcupera los datos de los productos desde Momgo por Producto*/
app.get ('/Productos/:idproducto', function(req,res){
  var urlProducto = urlProductos + '&q={"idProducto":"' + req.params.idproducto + '"}';
  //res.send(urlProducto);
  var productoMlab = requestjson.createClient(urlProducto);
  productoMlab.get('',function(err, resM, body){
    if (err){
      console.log(body);
    }else {
      res.send(body);
    }
  })
})

/* Valida el usuario en base a los parametros que envian desde app-login.html*/
app.post('/Login', function(req,res){
  res.set("Access-Control-Allow-Headers","Content-Type");
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';

  clienteMLabRaiz = requestjson.createClient(urlRaizMlab + "/Usuarios?" + apiKey + "&" + query);
  console.log(urlRaizMlab + "/Usuarios?" + apiKey + "&" + query);
  //res.status(200).send('Usuario logado');

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){ //login  ok
        res.status(200).send('Usuario logado');
      } else{
        res.status(404).send('Usuario no encontrado, registrese');
       }
    }
  })
})

// Registro de usuario nuevo
app.post('/Registro', function(req, res) {
        var email = req.body.email;
        var password = req.body.password;
        var nombre = req.body.nombre;
        var apellido = req.body.apellido;

        collection = db.collection("Clientes");
        collection.insert([{"idcliente" : 999, "nombre" : nombre ,"apellido": apellido,"email" : email,"estatus" : true}], function(err, doc) {
          console.log("Resultado Insert Registro: " + doc);
          collection.findOne({email:req.body.email}, function(err2, item) {
            if(err2) {
              console.log("Sin acceso a BD.");
              res.status(400).send("Registro .. noBD");
            }
            else {
              if (item == null ) {
                console.log("Registro fallidos.");
                return res.status(400).send("Registro fallidos");
              } else {
                console.log("Usuario " + item.nombre + " (" + item._id + ") creado correctamente.");
                return res.status(200).send("okINSERT");
              }
            }
            });
        });

}); // cierra POST registro
